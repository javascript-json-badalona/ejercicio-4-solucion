function modificaPares(elementos, fx) {
  let elementosModificados = [];
  for (let i in elementos) {
    if ((+i + 1) % 2 === 0) {
      elementosModificados.push(fx(elementos[i]));
    } else {
      elementosModificados.push(elementos[i]);
    }
  }
  return elementosModificados;
}

function capitaliza(palabra) {
  return palabra[0].toUpperCase() + palabra.slice(1);
}

let palabras = ['café', 'croissant', 'tostada', 'zumo'];

let palabras2 = modificaPares(palabras, elemento => `hola ${elemento}`);
let palabras3 = modificaPares(palabras, elemento => capitaliza(elemento));
console.log(palabras3);
